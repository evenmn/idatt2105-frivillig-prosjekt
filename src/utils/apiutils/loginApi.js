import axios from "axios";

export function generateToken (credentials) {
    return axios.post(`http://localhost:8080/api/token`, credentials)
    .then(response => {
        return response;
    })
}