import axios from "axios";

export function createCourse (course) {
    return axios.post(`http://localhost:8080/api/courses`, course)
    .then(response => {
        return response.data;
    })
}

export function getCourses () {
    return axios.get(`http://localhost:8080/api/courses`)
    .then(response => {
        return response.data;
    })
}

export function getCoursesByEmail (email) {
    return axios.get(`http://localhost:8080/api/courses?email=` + email)
    .then(response => {
        return response.data;
    })
}

export function getCoursesByAssistantEmail (email) {
    return axios.get(`http://localhost:8080/api/courses/assistant/` + email)
    .then(response => {
        return response.data;
    })
}

export function getCourseByCourseId (id) {
    return axios.get(`http://localhost:8080/api/courses/` + id)
    .then(response => {
        return response.data;
    })
}

export function getCourseWithoutCourseId (course) {
    return axios.post(`http://localhost:8080/api/coursesNoId/`, course)
    .then(response => {
        return response.data;
    })
}

export function getQueueSize (id) {
    return axios.get(`http://localhost:8080/api/courses/queue/` + id)
    .then(response => {
        return response.data;
    })
}

export function updateCourse (courseId, course) {
    return axios.put(`http://localhost:8080/api/courses/` + courseId, course)
    .then(response => {
        return response.data;
    })
}

export function deleteCourse (courseId) {
    return axios.delete(`http://localhost:8080/api/courses/` + courseId)
    .then(response => {
        return response.data;
    })
}

export function assignAssistant (courseId, assistantEmail) {
    return axios.post(`http://localhost:8080/api/courses/` + courseId + '/' + assistantEmail)
    .then(response => {
        return response.data;
    })
}

export function removeAssistant (courseId, assistantEmail) {
    return axios.delete(`http://localhost:8080/api/courses/assistant/` + courseId + '/' + assistantEmail)
    .then(response => {
        return response.data;
    })
}

export function removeAllAssistants (courseId) {
    return axios.delete(`http://localhost:8080/api/courses/assistant/` + courseId)
    .then(response => {
        return response.data;
    })
}