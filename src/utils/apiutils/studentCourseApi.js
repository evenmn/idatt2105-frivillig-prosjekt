import axios from "axios";

export function assignStudentToCourse (studentCourse) {
    return axios.post(`http://localhost:8080/api/sc`, studentCourse)
    .then(response => {
        return response.data;
    })
}

export function getAllStudentCourses () {
    return axios.get(`http://localhost:8080/api/sc`)
    .then(response => {
        return response.data;
    })
}

export function removeStudentFromCourse (studentCourse) {
    return axios.delete(`http://localhost:8080/api/sc`, studentCourse)
    .then(response => {
        return response.data;
    })
}

export function getObligs (studentCourse) {
    return axios.post(`http://localhost:8080/api/sc/obliglist`, studentCourse)
    .then(response => {
        return response.data;
    })
}

export function approveOblig (approval) {
    return axios.post(`http://localhost:8080/api/sc/oblig/`, approval)
    .then(response => {
        return response.data;
    })
    .catch(() => {
        return null
    })
}