import axios from "axios";

export function createQueueElement (queueElement) {
    return axios.post(`http://localhost:8080/api/queue`, queueElement)
    .then(response => {
        return response.data;
    })
}

export function getAllQueues () {
    return axios.get(`http://localhost:8080/api/queue`)
    .then(response => {
        return response.data;
    })
}

export function getQueuesByEmail (email) {
    return axios.get(`http://localhost:8080/api/queue?email=` + email)
    .then(response => {
        return response.data;
    })
}

export function getQueuesByCourseId (courseId) {
    return axios.get(`http://localhost:8080/api/queue?courseId=` + courseId)
    .then(response => {
        return response.data;
    })
}

export function getQueuesByCourseIdAndEmail (courseId, email) {
    return axios.get(`http://localhost:8080/api/queue?courseId=` + courseId + '&email=' + email)
    .then(response => {
        return response.data;
    })
}

export function getQueueByQueueId (queueId) {
    return axios.get(`http://localhost:8080/api/queue/` + queueId)
    .then(response => {
        return response.data;
    })
}

export function updateQueueElement (queueId, queueElement) {
    return axios.put(`http://localhost:8080/api/queue/` + queueId, queueElement)
    .then(response => {
        return response.data;
    })
}

export function deleteQueueElement (queueId) {
    return axios.delete(`http://localhost:8080/api/queue/` + queueId)
    .then(response => {
        return response.data;
    })
}

export function deleteAllQueueElements () {
    return axios.delete(`http://localhost:8080/api/queue`)
    .then(response => {
        return response.data;
    })
}

export function deleteAllQueueElementsFromCourse (courseId) {
    return axios.delete(`http://localhost:8080/api/queue?courseId=` + courseId)
    .then(response => {
        return response.data;
    })
}

export function assignAssistant (queueId, assistantEmail) {
    return axios.post(`http://localhost:8080/api/queue/` + queueId + '/' + assistantEmail)
    .then(response => {
        return response.data;
    })
}

export function removeAssistant (queueId) {
    return axios.delete(`http://localhost:8080/api/queue/` + queueId + '/assistants')
    .then(response => {
        return response.data;
    })
}