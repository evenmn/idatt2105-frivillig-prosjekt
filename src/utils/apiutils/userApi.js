import axios from "axios";

export function createUser (user, password) {
    return axios.post(`http://localhost:8080/api/users/` + password, user)
    .then(response => {
        return response.data;
    })
}

export function registerUsers (userInfo, courseId) {
    return axios.post(`http://localhost:8080/api/users/register/` + courseId, userInfo)
    .then(response => {
        return response.data;
    })
}

export function getUsers () {
    return axios.get(`http://localhost:8080/api/users`)
    .then(response => {
        return response.data;
    })
}

export function getUsersByCourseId (courseId) {
    return axios.get(`http://localhost:8080/api/users?courseId=` + courseId)
    .then(response => {
        return response.data;
    })
}

export function createAssistants () {
    return axios.post(`http://localhost:8080/api/users/assistants`)
    .then(response => {
        return response.data;
    })
}

export function getUserByEmail (email) {
    return axios.get(`http://localhost:8080/api/users/` + email)
    .then(response => {
        return response.data;
    })
}

export function updateUser (user, email) {
    return axios.put(`http://localhost:8080/api/users/` + email, user)
    .then(response => {
        return response.data;
    })
}

export function updatePassword (credentials) {
    return axios.put(`http://localhost:8080/api/users/`, credentials)
    .then(response => {
        return response.data;
    })
}

export function deleteUser (email) {
    return axios.delete(`http://localhost:8080/api/users/` + email)
    .then(response => {
        return response.data;
    })
}

export function deleteAllUsers () {
    return axios.delete(`http://localhost:8080/api/users`)
    .then(response => {
        return response.data;
    })
}

export function getAdmins () {
    return axios.get(`http://localhost:8080/api/users/admins`)
    .then(response => {
        return response.data;
    })
}

export function findStudents () {
    return axios.get(`http://localhost:8080/api/users/students`)
    .then(response => {
        return response.data;
    })
}

export function validatePassword (credentials) {
    console.log(credentials)
    return axios.post(`http://localhost:8080/api/validation`, credentials)
    .then(response => {
        return response.data;
    })
}