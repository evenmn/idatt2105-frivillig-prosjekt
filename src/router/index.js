import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/settings',
    name: 'settings',
    component: () => import(/* webpackChunkName: "about" */ '../views/SettingsView.vue')
  },
  {
    path: '/queue/:courseId',
    name: 'queue',
    props : true,
    component: () => import(/* webpackChunkName: "about" */ '../views/queue/StudentQueue.vue')
  },
  {
    path: '/form/:courseId',
    name: 'form',
    props: true,
    component: () => import(/* webpackChunkName: "about" */ '../views/QueueForm.vue')
  },
  {
    path: '/exerciseView/:courseId/:userEmail',
    name: 'exerciseView',
    props: true,
    component: () => import(/* webpackChunkName: "about" */ '../views/ExerciseView.vue'),
    meta:{
      header:1,
    }
  },
  {
    path: '/login' ,
    name: 'login',
    component: () => import(/* webpackChunkName: "about" */ '../views/LoginView.vue')
  },
  {
    path: '/adminHome' ,
    name: 'adminHome',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminHomeView.vue')
  },
  {
    path: '/adminArchive' ,
    name: 'adminArchive',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminArchive.vue')
  },
  {
    path: '/adminUserList' ,
    name: 'adminUserList',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminUserList.vue')
  },
  {
    path: '/adminCreate' ,
    name: 'adminCreate',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminCreateView.vue')
  },
  {
    path: '/adminCreateUser' ,
    name: 'adminCreateUser',
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminCreateUser.vue')
  },
  {
    path: '/adminStudents/:courseId' ,
    name: 'adminStudents',
    props: true,
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminStudentsView.vue')
  },
  {
    path: '/adminEdit/:courseId' ,
    name: 'adminEdit',
    props: true,
    component: () => import(/* webpackChunkName: "about" */ '../views/admin/adminEditView.vue')
  },
  {
    path: '/courseArchive' ,
    name: 'courseArchive',
    component: () => import(/* webpackChunkName: "about" */ '../views/archiveView.vue')
  },
  {
    path: '/studasshome' ,
    name: 'studasshome',
    component: () => import(/* webpackChunkName: "about" */ '../views/StudassHome.vue')
  },
  {
    path: '/forgotpassword' ,
    name: 'forgotpassword',
    component: () => import(/* webpackChunkName: "about" */ '../views/ForgotPassword.vue')
  },
  {
    path: '/studassform/:queueId' ,
    name: 'studassform',
    props: true,
    component: () => import(/* webpackChunkName: "about" */ '../views/studassForm.vue')
  },



]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
