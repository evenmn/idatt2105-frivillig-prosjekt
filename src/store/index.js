import { createStore } from 'vuex'
import axios from 'axios'
import { getUserByEmail } from '@/utils/apiutils/userApi'
import { generateToken } from '@/utils/apiutils/loginApi'
import { getCoursesByEmail } from '@/utils/apiutils/courseApi'
import { getCoursesByAssistantEmail } from '@/utils/apiutils/courseApi'

export default createStore({
  state: {
    /**
     * Objekt med info om den innloggede brukere
     * String userEmail
     * String firstName
     * String lastName
     * boolean admin
     */
    user : {},

    courses: [],
    assistedCourses: [],

    token : null
  },
  getters: {
    loggedIn (state) {
      return !!state.token
    }
  },
  mutations: {
    SET_TOKEN_DATA (state, token) {
      state.token = token
      localStorage.setItem('token', JSON.stringify(token))
      axios.defaults.headers.common['Authorization'] = `Bearer ${
        token.data
      }`
    },
    SET_USER_DATA (state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
      console.log("logged in as " + userData.firstName + " " + userData.lastName)
    },
    SET_COURSE_DATA (state, courseData) {
      for (const course in courseData) {
        state.courses.push(courseData[course].courseId)
      }
      localStorage.setItem('courses', JSON.stringify(state.courses))
    },
    SET_ASSISTANT_DATA (state, assistantData) {
      for (const course in assistantData) {
        state.assistedCourses.push(assistantData[course].courseId)
      }
      localStorage.setItem('assistedCourses', JSON.stringify(state.assistedCourses))
    },
    CLEAR_DATA () {
      localStorage.removeItem('token')
      localStorage.removeItem('user')
      localStorage.removeItem('courses')
      localStorage.removeItem('assistedCourses')
      location.reload()
    },
    RESTORE_DATA (state) {
      const tokenString = localStorage.getItem('token')
      const userString = localStorage.getItem('user')
      const courseString = localStorage.getItem('courses')
      const assistantString = localStorage.getItem('assistedCourses')
      if (userString && tokenString) {
        const tokenData = JSON.parse(tokenString)
        const userData = JSON.parse(userString)
        const courseData = JSON.parse(courseString)
        const assistantData = JSON.parse(assistantString)
        this.state.token = tokenData
        axios.defaults.headers.common['Authorization'] = `Bearer ${
          tokenData.data
        }`
        state.user = userData
        state.courses = courseData
        state.assistedCourses = assistantData
      }
    }
  },
  actions: {
    async login ({ commit }, credentials) {
      await generateToken(credentials).then((token) => {
        if (token.data == "Access denied") {
          console.log("login failed")
          return
        }
        commit('SET_TOKEN_DATA', token);
      })

      if(this.state.token) {
        await getUserByEmail(credentials.userEmail).then(( user ) => {
          commit('SET_USER_DATA', user)
        })
        await getCoursesByEmail(credentials.userEmail).then(( courses ) => {
          commit('SET_COURSE_DATA', courses)
        })
        await getCoursesByAssistantEmail(credentials.userEmail).then(( courses ) => {
          commit('SET_ASSISTANT_DATA', courses)
        })
      }
    },

    logout ({ commit }) {
      commit('CLEAR_DATA')
    }
  },
  modules: {
  }
})
