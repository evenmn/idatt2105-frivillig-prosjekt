import{ mount} from  "@vue/test-utils";
import LoginView from "@/views/LoginView";
import store from "@/store";
//skulle lagt til brukeren
describe("LoginForm", () => {

    test("Feil passord", async () => {
        const wrapper = mount(LoginView);

        await wrapper.find('#username').setValue("ola.nordmann@gmail.com");
        await wrapper.find('#password').setValue("123123123");
        await wrapper.find('#loginButton').trigger("click");

        expect(wrapper.find('#errorMessage')).toBeTruthy();
    });


    test("Feil brukernavn", async () => {
        const wrapper = mount(LoginView);

        await wrapper.find('#username').setValue("ola.nordmann@gmail.com123");
        await wrapper.find('#password').setValue("123");
        await wrapper.find('#loginButton').trigger("click");

        expect(wrapper.find('#errorMessage')).toBeTruthy();
    });

    test("Riktig Innlogging", async () => {
        const wrapper = mount(LoginView);

        await wrapper.find('#username').setValue("ola.nordmann@gmail.com");
        await wrapper.find('#password').setValue("1234");
        await wrapper.find('#loginButton').trigger("click");

        expect(wrapper.find('#errorMessage')).toBeTruthy();
    });

});
